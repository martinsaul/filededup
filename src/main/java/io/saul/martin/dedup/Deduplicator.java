package io.saul.martin.dedup;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.*;

public class Deduplicator {
	private final File targetFolder;
	private final ExecutorService executorService;
	private final HashMap<Long, HashMap<String, ArrayList<File>>> colisions;
	private final BlockingQueue<Future<?>> futures;

	public Deduplicator(String path) {
		this.futures = new LinkedBlockingQueue<>();
		this.targetFolder = new File(path);
		if(!targetFolder.exists())
			throw new RuntimeException("Target folder does not exist.");
		if(!targetFolder.isDirectory())
			throw new RuntimeException("Target is not a folder.");
		this.executorService = new ThreadPoolExecutor(5, 10, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
		colisions = new HashMap<>();
	}

	public HashMap<String, ArrayList<File>> run() {
		publishFolder(targetFolder);
		try {
			while(!futures.isEmpty()) {
				try {
					Future<?> future = futures.poll(10, TimeUnit.SECONDS);
					if(future!=null)
						future.get();
				} catch (ExecutionException ignore){}
			}

			HashMap<String, ArrayList<File>> results = new HashMap<>();
			for(HashMap<String, ArrayList<File>> hashMap: colisions.values()){
				for(String key: hashMap.keySet()) {
					ArrayList<File> map = hashMap.get(key);
					if (map.size() > 1){
						results.computeIfAbsent(key, s -> new ArrayList<>());
						results.get(key).addAll(map);
					}
				}
			}
			return results;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			try {
				executorService.shutdown();
				executorService.awaitTermination(10, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public void publishFolder(File targetFolder) {
		submit(new FolderAnalysis(this, targetFolder));
	}

	public void publishFile(File file) {
		submit(new FileAnalysis(this, file));
	}

	public void submit(Runnable runnable){
		FutureTask<Void> ft = new FutureTask<Void>(runnable, null);
		futures.add(executorService.submit(ft));
	}
	public void register(File file, long length, String shaChecksum) {
		HashMap<String, ArrayList<File>> shaMap = colisions.computeIfAbsent(length, (k) -> new HashMap<>());
		ArrayList<File> redundantFileList = shaMap.computeIfAbsent(shaChecksum, (k) -> new ArrayList<>());
		redundantFileList.add(file);
	}

}
