package io.saul.martin.dedup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;

public class FileAnalysis implements Runnable {
	private final Deduplicator deduplicator;
	private final File file;

	public FileAnalysis(Deduplicator deduplicator, File file) {
		this.deduplicator = deduplicator;
		this.file = file;
	}

	@Override
	public void run() {
		System.out.println("Analysing file: " + file.getAbsolutePath());
		try {
			MessageDigest shaDigest = MessageDigest.getInstance("SHA-1");
			String shaChecksum = getFileChecksum(shaDigest, file);

			deduplicator.register(file, file.length(), shaChecksum);
		} catch (Exception ex) {
			System.err.println("Failed to parse " + file.getAbsolutePath());
		}
	}

	private static String getFileChecksum(MessageDigest digest, File file) throws IOException
	{
		//Get file input stream for reading the file content
		FileInputStream fis = new FileInputStream(file);

		//Create byte array to read data in chunks
		byte[] byteArray = new byte[1024];
		int bytesCount = 0;

		//Read file data and update in message digest
		while ((bytesCount = fis.read(byteArray)) != -1) {
			digest.update(byteArray, 0, bytesCount);
		};

		//close the stream; We don't need it now.
		fis.close();

		//Get the hash's bytes
		byte[] bytes = digest.digest();

		//This bytes[] has bytes in decimal format;
		//Convert it to hexadecimal format
		StringBuilder sb = new StringBuilder();
		for(int i=0; i< bytes.length ;i++)
		{
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		//return complete hash
		return sb.toString();
	}
}
