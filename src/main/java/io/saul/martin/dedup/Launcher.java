package io.saul.martin.dedup;

public class Launcher {

	public static void main(String[] args) {
		Deduplicator dup;
		switch (args.length){
			case 0:
				System.err.println("No folder specified. Please try again passing a folder as a parameter.");
				break;
			case 1:
				dup = new Deduplicator(args[0]);
				dup.run();
				break;
			default:
				String fullPath = String.join(" ", args);
				fullPath = fullPath.substring(fullPath.indexOf("\""), fullPath.lastIndexOf("\""));
				dup = new Deduplicator(fullPath);
				dup.run();
				break;
		}
	}

}
