package io.saul.martin.dedup;

import java.io.File;

public class FolderAnalysis implements Runnable {
	private final Deduplicator deduplicator;
	private final File targetFolder;

	public FolderAnalysis(Deduplicator deduplicator, File targetFolder) {
		this.deduplicator = deduplicator;
		this.targetFolder = targetFolder;
	}

	@Override
	public void run() {
		if(targetFolder.exists() & targetFolder.isDirectory())
			System.out.println("Analysing folder: " + targetFolder.getAbsolutePath());
			for(File file: targetFolder.listFiles()){
				if(file.isDirectory())
					deduplicator.publishFolder(file);
				else
					deduplicator.publishFile(file);
			}
	}
}
